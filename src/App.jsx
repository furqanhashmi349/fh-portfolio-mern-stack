import { Route, Routes } from "react-router-dom"
import { Navbar2 } from "./Components/Navbar2"
import Home from "./Components/Home"
import About from "./Components/About"
import Contact from "./Components/Contact"
import './App.css'
import Skill from "./Components/Skill"
import { Project } from "./Components/Project"
import Footer from "./Components/Footer"



function App() {

  return (
    <div>
      <Navbar2/>
 <Routes>
  <Route path="/" element={<Home/>}></Route>
  <Route path="/about" element={<About/>}></Route>
  <Route path="/contact" element={<Contact/>}></Route>
  <Route path="/project" element={<Project/>}></Route>
  <Route path="/skill" element={<Skill/>}></Route>
 </Routes>
   <Footer/>

  
    </div>
  )
}

export default App
